const express   = require('express');
const cookie   = require('cookie-parser');
const app       = express();
const requestIP = require('request-ip');
var fs = require('fs');

app.use(cookie());
app.use(requestIP.mw());

app.get('/', (req, res) => {

    let cookieValue = req.cookies.cookieName;
    let disp;
    if (cookieValue) {
        disp = `<h1>Welcome back ${cookieValue} </h1>`;
        disp += `<a href="/delete_cookie">delete cookie</a>`;
    } else {
        disp = `<h1>Welcome</h1>`;
        disp += `<a href="/set_cookie">set cookie</a>`;
    }

    res.send(disp);
    }
);

//set_cookie
app.get('/set_cookie', (req, res) => {
    res.cookie('cookieName', 'Shanawaz', { maxAge: 900000, httpOnly: true });
    res.redirect('/');
}
);

// delete_cookie
app.get('/delete_cookie', (req, res) => {
    res.clearCookie('cookieName');
    res.redirect('/');
});



// 2) get IP Address

app.get('/ip', (req, res) => {

    let ip = requestIP.getClientIp(req);
    res.send(`<h1>Your IP Address is ${ip}</h1>`);
}
);

// 3) Read a File in NodeJS
app.get('/file', (req, res) => {
    fs.readFile('readme.md', 'utf8', function (err, data) {
        if (err) throw err;
        res.send(data);

    }
    );
}
);





app.listen(3000, () => {
    console.log('Example app listening on port 3000!');
}
);